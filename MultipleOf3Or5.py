# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.

def multiple_of_3(num):
    return (num%3 == 0)

def multiple_of_5(num):
    return (num%5 == 0)

sum = 0
for number in range (0, 1000):
    if (multiple_of_3(number) or multiple_of_5(number)):
        sum += number


print(sum)