# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
# Greatest Common Divisor
def GreatestCommonDivisor(x,y):
    return y and GreatestCommonDivisor(y, x % y) or x
# Lowest Common Denominator
def LowestCommonDenominator(x,y):
    return x * y / GreatestCommonDivisor(x,y)

n = 1
for i in range(1, 20):
    n = LowestCommonDenominator(n, i)
print(int(n))